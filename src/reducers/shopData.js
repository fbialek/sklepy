import { RECEIVE_DATA } from '../actions/shopDataAction';

const initialState = {
  shops: []
};

const shopData = (state = initialState, action) => {
  switch (action.type) {
    case RECEIVE_DATA:
      return Object.assign({}, state, {
        shops: action.shopData
      });
    default:
      return state;
    }
};

export default shopData;
