import { CENTER_MAP } from '../actions/listActions';

const list = (state = {}, action) => {
  switch (action.type) {
  case CENTER_MAP:
    return Object.assign({}, state, {
      coords: action.coords,
      id: action.id
    });
  default:
    return state;
  }
};

export default list;
