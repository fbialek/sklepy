import { combineReducers } from 'redux';
import shopData from './shopData';
import list from './list';

const rootReducer = combineReducers({
  shopData,
  list
});

export default rootReducer;
