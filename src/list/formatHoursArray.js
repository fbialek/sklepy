export default function (arr) {
  // translate and sort array of hours
  const polishDaysOfWeek = [
    ['Friday', 'Piątek', 5],
    ['Monday', 'Poniedziałek', 1],
    ['Saturday', 'Sobota', 6],
    ['Sunday', 'Niedziela', 0],
    ['Thursday', 'Czwartek', 4],
    ['Tuesday', 'Wtorek', 2],
    ['Wednesday', 'Środa', 3]
  ];
  const result = [];
  for (let [index, value] of polishDaysOfWeek.entries()) {
    result[value[2]] = [polishDaysOfWeek[index][1], arr[index][1]];
  }
  return result;
}
