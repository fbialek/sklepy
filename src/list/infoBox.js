import React from 'react';
import formatHoursArray from './formatHoursArray';
import checkIfOpen from './checkIfOpen';
import '../styles/InfoBox.css';

const InfoBox = ({hours, imgURL}) => {
  const handleError = (e) => {
    e.target.src = 'https://upload.wikimedia.org/wikipedia/commons/thumb/3/3d/Map-icon-shop.svg/644px-Map-icon-shop.svg.png';
  }
  const hoursArray = Object.entries(hours);
  const formattedHoursArray = formatHoursArray(hoursArray);
  const hoursList = formattedHoursArray.map((item, idx) =>
    <li className="info-item" key={idx}>{item[0]}: {item[1]}</li>);
  const infoIfOpen = checkIfOpen(formattedHoursArray);
  return (
    <div className="info-box">
      <img className="info-img" src={imgURL} onError={handleError} alt="" />
      <ul className="info-list">
        {hoursList}
      </ul>
      <p className="info-open">{infoIfOpen}</p>
    </div>
  );
};

export default InfoBox;
