import React from 'react';
import InfoBox from './infoBox';
import '../styles/ListItem.css';

const ListItem = ({data, id, clickedID, handleClick}) => {
  const coords = [parseFloat(data.latitude), parseFloat(data.longitude)];
  const isClicked = id === clickedID;
  return (
    <li className={"list-item " + ( isClicked ? "clicked" : "")} onClick={() => handleClick(coords, id)}>
          <h3 className="list-header">{data.siec}</h3>
          <p>{data.adres}</p>
          {isClicked && <InfoBox imgURL={data.logo} hours={data.godziny[0]} /> }
        </li>
  );
};

export default ListItem;
