import checkIfOpen from './checkIfOpen';

const shopsOpen = [
  ['day', '01:00 - 23:59'],
  ['day', '01:00 - 23:59'],
  ['day', '01:00 - 23:59'],
  ['day', '01:00 - 23:59'],
  ['day', '01:00 - 23:59'],
  ['day', '01:00 - 23:59'],
  ['day', '01:00 - 23:59'],
];

const unkonwnTimes = [
  ['day', 'brak danych'],
  ['day', 'brak danych'],
  ['day', 'brak danych'],
  ['day', 'brak danych'],
  ['day', 'brak danych'],
  ['day', 'brak danych'],
  ['day', 'brak danych'],
];

test('shops open all the time', () =>{
  expect(checkIfOpen(shopsOpen)).toBe('Sklep jest czynny. Zapraszamy!');
});
test('no data provided', () =>{
  expect(checkIfOpen(unkonwnTimes)).toBe('');
});
