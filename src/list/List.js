import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Scrollbars } from 'react-custom-scrollbars';
import ReactGA from 'react-ga';
import { centerMap } from '../actions/listActions';
import ListItem from './ListItem';
import '../styles/List.css';

class List extends Component {

  render() {
    const items = this.props.data.map((item, idx) =>
      <ListItem data={item} id={idx} key={idx} clickedID={this.props.clickedID} handleClick={this.props.onListItemClick} />
    );
    return (
      <div className="listContainer">
        <Scrollbars>
          <ul className="list">
            {items}
          </ul>
        </Scrollbars>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    clickedID: state.list.id
  }
};

function mapDispatchToProps(dispatch) {
  return {
    onListItemClick: (coords, id) => {
      ReactGA.event({
        category: 'User',
        action: 'clicked on list',
        value: id
      });
      dispatch(centerMap(coords, id));
    }
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(List);
