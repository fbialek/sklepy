function changeStringtoTime (str) {
  switch (str) {
  case '24h':
    return 1; 
  case 'brak danych':
    return '';
  default:
    break;
  }
  const strToArray = str.split(' ');
  const time = {};
  const openHoursMinutes = [parseInt(strToArray[0].split(':')[0], 10), parseInt(strToArray[0].split(':')[1], 10)];
  const closeHoursMinutes = [parseInt(strToArray[2].split(':')[0], 10), parseInt(strToArray[2].split(':')[1], 10)];
  time.open = openHoursMinutes;
  time.close = closeHoursMinutes;
  // check if hours and minutes are corr08:00 - 20:00ect
  // hours are in range 0-23, minutes in range 0-59
  const correctOutput = time.open[0] >= 0
                        && time.open[0] < 24
                        && time.close[0] >= 0
                        && time.close[0] < 24
                        && time.open[1] >= 0
                        && time.open[1] <= 59
                        && time.close[1] >= 0
                        && time.close[1] <= 59;
  if (!correctOutput) {
    return 0; 
  } else {
    return time;
  }
}
export default function (arr) {
  const now = new Date();
  const minutes = now.getMinutes();
  const hours = now.getHours();
  let day = now.getDay();
  if (day === 0) {
    day = 7;
  }
  const time = changeStringtoTime(arr[day - 1][1]);
  switch (time) {
  case '':
    return '';
  case 0:
    return 'Sklep jest teraz nieczynny.';
  case 1:
    return 'Sklep jest czynny. Zapraszamy!';
  default:
    if (hours < time.open[0] || hours > time.close[0]) {
      return 'Sklep jest teraz nieczynny.';
    } else if (hours === time.open[0] && minutes < time.open[1]) {
      return 'Sklep jest teraz nieczynny.';
    } else if (hours === time.close[0] && minutes > time.close[1]) {
      return 'Sklep jest teraz nieczynny.';
    } else {
      return 'Sklep jest czynny. Zapraszamy!';
    }
  }

}
