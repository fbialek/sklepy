import axios from 'axios';
import { jsonURL } from '../config';

export const RECEIVE_DATA = 'RECEIVE_DATA';

function receiveData (response) {
  return {
    type: RECEIVE_DATA,
    shopData: response
  };
}

function fetchShopData() {
  return dispatch => {
    return axios.get(jsonURL)
      .then(response => {
        return response.data;
      })
      .then(json => dispatch(receiveData(json)))
      .catch(error => console.log(error));
  };
}

export function fetchDataIfNeeded () {
  return (dispatch, getState) => {
    return dispatch(fetchShopData());
  };
}
