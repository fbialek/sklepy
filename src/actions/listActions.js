export const CENTER_MAP = 'CENTER_MAP';

export const showMoreData = id => {
  return {
    type: 'SHOW_MORE_DATA',
    id
  };
};

export const centerMap = (coords, id) => {
  return {
    type: 'CENTER_MAP',
    coords,
    id
  };
};
