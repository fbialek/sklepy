import { defaultImage } from '../config';

export default function(shopObj) {
  return `<div class="infoWindow"><p class="siec">${shopObj.siec}</p><p class="address">${shopObj.adres}</p><img class="logo" src="${shopObj.logo}" alt="${shopObj.siec} logo" onerror="this.onerror=null;this.src='${defaultImage}';"></div>`;
}
