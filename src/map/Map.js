import React, { Component } from 'react';
import { connect } from 'react-redux';
import scriptLoader from 'react-async-script-loader';
import ReactGA from 'react-ga';
import { googleApiKey } from '../config';
import { fetchDataIfNeeded } from '../actions/shopDataAction';
import generateInfoWindow from './generateInfoWindow';
import List from '../list/List';
import '../styles/Map.css';

@scriptLoader(["https://maps.googleapis.com/maps/api/js?key=" + googleApiKey])
class Map extends Component {
  constructor (props) {
    super(props);
    this.map = null;
    this.markers = [];
    this.state = {
      isMapRendered: false
    }
  }

  componentWillReceiveProps ({ isScriptLoaded, isScriptLoadSucceed, data, coords, id }) {
    const that = this;
    if (isScriptLoaded && data.length && !this.state.isMapRendered && (typeof window !== 'undefined')) {
        this.map = new window.google.maps.Map(this.refs.map, {
          center: {lat: 52.244234, lng: 21.001841},
          zoom: 16
        })
        this.infoWindow = new window.google.maps.InfoWindow();
        data.forEach(item => {
          let marker = new window.google.maps.Marker({
            position: {lat: parseFloat(item.latitude), lng: parseFloat(item.longitude)},
            map: that.map
          });
          window.google.maps.event.addListener(marker, 'click', function () {
            that.infoWindow.setContent(generateInfoWindow(item));
            that.infoWindow.open(that.map, this);
            that.map.setCenter({lat: parseFloat(item.latitude), lng: parseFloat(item.longitude)});
            ReactGA.event({
              category: 'User',
              action: 'clicked on map',
              label: data.siec
            });
          });
          that.markers.push(marker);
        })
      this.setState({
        isMapRendered: true
      });
    }
    if (coords) {
      this.map.setCenter({lat: coords[0], lng: coords[1]});
      that.markers[id].setAnimation(window.google.maps.Animation.BOUNCE);
      setTimeout(() => {
        that.markers[id].setAnimation(null);
      }, 2800)
    }
    if (id) {
      this.infoWindow.close();
    }
  }

  componentDidMount () {
    const { dispatch } = this.props;
    dispatch(fetchDataIfNeeded());
  }

  render() {
    return (
      <div className="mapContainer">
        <div ref="map" className="map"></div>
        <List data={this.props.data} />
      </div>
    );
  }
}

function mapStateToProps (state) {
  return {
    data: state.shopData.shops,
    coords: state.list.coords,
    id: state.list.id
  };
}
export default connect(mapStateToProps)(Map);
