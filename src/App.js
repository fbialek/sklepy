import React, { Component } from 'react';
import Map from './map/Map';
import ReactGA from 'react-ga';
import { gaID } from './config';
import './styles/App.css';

if (typeof window !== 'undefined') {
  ReactGA.initialize(gaID);
}

class App extends Component {

  componentDidMount () {
    if (typeof window !== 'undefined') {
      ReactGA.pageview(window.location.pathname);
    }
  }

  render() {
    return (
      <div className="App">
        <Map />
      </div>
    );
  }
}

export default App;
